FROM nginx:1.23-alpine

RUN apk update && apk add --no-cache openssh rsync shadow

COPY default.conf /etc/nginx/conf.d/

ADD src/Skeleton-2.0.4 /usr/share/nginx/html/dockerrunner/

COPY build/src/index.js* /usr/share/nginx/html/dockerrunner/

EXPOSE 80
