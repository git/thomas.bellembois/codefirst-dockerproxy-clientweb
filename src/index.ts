// const proxyUrl = "http://localhost:8080";
// const proxyPath = "/";

const proxyUrl = "";
const proxyPath = "/dockerrunner/";

// console.log(proxyUrl);

function getContainers() {
  const httpRequest = new XMLHttpRequest();

  if (!httpRequest) {
    alert('Can not create httpRequest.');
    return false;
  }

  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        getContainersSuccess(httpRequest.responseText);
      } else {
        getContainersError(httpRequest.responseText);
      }
    }
  };
  httpRequest.open('GET', proxyUrl + proxyPath + 'containers/json');
  httpRequest.send();

  return '';
}

function getContainerLog(this: HTMLElement, ev: Event) {
  const containerId = this.getAttribute('id');
  const httpRequest = new XMLHttpRequest();

  if (!httpRequest) {
    alert('Can not create httpRequest.');
    return false;
  }

  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        getContainerLogSuccess(httpRequest.responseText);
      } else {
        getContainerLogError(httpRequest.responseText);
      }
    }
  };
  httpRequest.open(
    'GET',
    proxyUrl + proxyPath + 'containers/' + containerId + '/logs'
  );
  httpRequest.send();

  return '';
}

function removeContainer(this: HTMLElement, ev: Event) {
  const containerId = this.getAttribute('id');
  const httpRequest = new XMLHttpRequest();

  if (!httpRequest) {
    alert('Can not create httpRequest.');
    return false;
  }

  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 204) {
        removeContainerSuccess(containerId!);
      } else {
        removeContainerError(containerId!);
      }
    }
  };
  httpRequest.open('DELETE', proxyUrl + proxyPath + 'containers/' + containerId);
  httpRequest.send();

  return '';
}

function getContainersSuccess(response: string) {
  const containersDiv = document.getElementById('containers');
  const containers = JSON.parse(response);

  if (containers.length === 0) {
    if (containersDiv !== null) {
      containersDiv.innerHTML = 'You have no running containers.';
    }
  } else {
    if (containersDiv !== null) {
      containersDiv.innerHTML = ' ';
    }
  }

  for (let index = 0; index < containers.length; index++) {
    const container = containers[index];
    const containerId = container.Id;
    const containerImage = container.Image;
    const containerSize = container.SizeRootFs;
    const containerName = container.Labels['codefirst-containername'];
    const containerEndpoint = container.Labels['codefirst-container-endpoint'];
    const containerPrivate = JSON.parse(container.Labels['codefirst-private']);

    let nf = new Intl.NumberFormat('en-US');
    const formattedContainerSize = nf.format(containerSize); // "1,234,567,890"

    // Main div.
    const containerDiv = document.createElement('div');
    containerDiv.setAttribute('class', 'row');
    containerDiv.setAttribute('id', containerId);

    // Container informations.
    const containerInfo = document.createElement('div');
    containerInfo.setAttribute(
      'style',
      'border-bottom: 1px dotted grey; font-family: fixed;'
    );
    containerInfo.setAttribute('class', 'eight columns');

    // Image name.
    const imageNameDiv = document.createElement('div');
    imageNameDiv.setAttribute('class', 'three columns');
    imageNameDiv.innerHTML = `[${containerImage}]`;

    // Container size.
    const containerSizeDiv = document.createElement('div');
    containerSizeDiv.setAttribute('class', 'two columns');
    containerSizeDiv.innerHTML = ` size: ${formattedContainerSize}`;


    // Container name.
    const containerNameDiv = document.createElement('div');
    containerNameDiv.setAttribute('class', 'five columns');

    if (containerPrivate) {
      containerNameDiv.innerHTML = ` private container: ${containerEndpoint}`;
    } else {
      const containerEndpointLink = document.createElement('a');
      containerEndpointLink.setAttribute('href', `${containerEndpoint}`);
      containerEndpointLink.setAttribute('target', '_blank');
      containerEndpointLink.innerHTML = `${containerEndpoint}`;

      containerNameDiv.appendChild(containerEndpointLink);
    }

    // Build action buttons.
    const containerLogs = document.createElement('button');
    containerLogs.setAttribute('id', containerId);
    containerLogs.setAttribute('class', '');
    containerLogs.innerHTML = 'logs';
    containerLogs.addEventListener('click', getContainerLog);

    const containerdDelete = document.createElement('button');
    containerdDelete.setAttribute('id', containerId);
    containerdDelete.setAttribute('class', '');
    containerdDelete.innerHTML = 'remove';
    containerdDelete.addEventListener('click', removeContainer);

    // Final layout.
    containerDiv.appendChild(imageNameDiv);
    containerDiv.appendChild(containerSizeDiv);
    containerDiv.appendChild(containerNameDiv);
    containerDiv.appendChild(containerLogs);
    containerDiv.appendChild(containerdDelete);

    containersDiv?.appendChild(containerDiv);
  }
}

function getContainersError(response: string) {
  console.log(response);
}

function getContainerLogSuccess(response: string) {
  const logDiv = document.getElementById('log');

  var newResponse = response.replaceAll('\u0002', "\n");
  newResponse = newResponse.replaceAll('\u0001', "\n");

  if (logDiv !== null) {
    logDiv.innerHTML = ' ';
    logDiv.appendChild(document.createTextNode(newResponse));
  }
}

function getContainerLogError(response: string) {
  console.log(response);
}

function removeContainerSuccess(containerId: string) {
  const containerDiv = document.getElementById(`${containerId}`);

  if (containerDiv !== null) {
    containerDiv.remove();
  }
}

function removeContainerError(containerName: string) {
  console.log(containerName);
}

window.onload = () => {
  getContainers();
};
