current_dir = $(shell pwd)

compile:
	docker run --rm -it --volume $(current_dir):/app sandrokeil/typescript tsc -p /app/tsconfig.json

build-image: compile
	docker build -t hub.codefirst.iut.uca.fr/thomas.bellembois/codefirst-dockerproxy-clientweb .

run-container: build-image
	docker run -p 8081:80 hub.codefirst.iut.uca.fr/thomas.bellembois/codefirst-dockerproxy-clientweb